import 'reflect-metadata'; // required by typedi

import express from 'express';
import cors from 'cors';
import { PingController } from './controllers';

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(express.json());

// add before middlewares here

// add controllers here
app.use('/', PingController);

// add after middlewares here

app.listen(port, () => {
  console.log(`application is running on port ${port}.`);
});
