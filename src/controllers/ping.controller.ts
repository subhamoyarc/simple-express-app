import { Router, Request, Response } from 'express';
import { PingService } from '../services';
import { Container } from 'typedi';

const router = Router();
const service = Container.get(PingService);

router.route('/ping').get((_req: Request, res: Response) => {
  res.status(200).send(service.ping());
})

export const PingController = router;
