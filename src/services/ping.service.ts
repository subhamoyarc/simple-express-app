import BaseService from './base.service';
import { PingRepository } from '../repositories';
import { Service } from 'typedi';

@Service()
export class PingService extends BaseService {
  constructor (private repository: PingRepository) {
    super();
  }

  ping () {
    return this.repository.ping();
  }
}
