import BaseRepository from './base.repository';
import { Service } from 'typedi';

@Service()
export class PingRepository extends BaseRepository {
  constructor () {
    super();
  }
  ping () {
    return 'pong';
  }
}
